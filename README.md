# Scientific Python SISEO #2

Scientific Python tutorial @ SISEO. Session 2.

Needs:
- [x] : data processing (csv, xls)
- [x] : stats (pandas, ...)
- [x] : graph theory
- [x] : fit data (scipy, matplotlib)
- [x] : MD, simulations
- [ ] : Work with meshes for CAD
- [x] : plots (publication ready)
- [x] : image processing (scales, basic stuff)
- [ ] : ?
